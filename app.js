class App {
    constructor(theme, tabs, text) {
        this.theme = 'app--light';
        this.activeTab;
        this.tabs = tabs || [new Elem('tab', new Tab())];
        this.buttonInactive = '';
        
        this.text = text || '';
        this.height = 0;

        this.setActiveTab();
    }

    setActiveTab(idx) {
        idx = idx || this.tabs[0].props.idx;

        this.tabs.map((x) => {
            x.props.activeClass = '';

            if (x.props.idx == idx) {
                x.props.activeClass = 'tab--active';
                this.text = x.props.text;
            }
        });

        this.activeTab = idx;

        let tab = this.getActiveTab();
        this.text = tab.props.text;
        
        this.toggleButtonInactive(this.text.length);
    }

    toggleButtonInactive(active) {
        this.buttonInactive = active ? '' : 'button--inactive';
    }

    getActiveTab(returnIndex) {
        returnIndex = returnIndex || false;

        return this.tabs.reduce((x, y, z) => {
            if (y.props.idx != this.activeTab) {
                return x;
            }

            return returnIndex ? z : y;
        }, null);
    }

    save() {
        let data = {
            theme: this.theme,
            activeTab: this.activeTab
        };

        data.tabs = this.tabs.reduce((x, y) => {
            x.push(y.props.store());
            return x;
        }, []);

        window.localStorage.texi = JSON.stringify(data);
    }

    load() {
        if (typeof window.localStorage.texi === 'undefined') {
            return;
        }

        let data = JSON.parse(window.localStorage.texi);

        this.theme = data.theme;
        this.activeTab = data.activeTab;

        app.del('tabs');
        app.set('tabs', []);

        this.tabs = data.tabs.reduce((x, y) => {
            x.push(new Elem('tab', new Tab(y.idx, y.name, y.text, y.activeClass)))
            return x;
        }, []);

        this.setActiveTab(data.activeTab);
        app.props.autosize({target: document.querySelector('.text__area')}, app);
        app.renderer.update();
    }

    addTab(ev, me) {
        let tabs = me.get('tabs');

        app.get('tabs').map((x) => {
            x.props.activeClass = '';
        });


        let tab = new Tab();
        tabs.push(new Elem('tab', tab));

        me.set('tabs', tabs);

        me.props.setActiveTab(tab.idx);
        me.renderer.update();

        me.props.save();
    }

    download(ev, me) {
        if (!me.props.text.length) {
            return;
        }

        let file = new Blob([me.props.text], {type: 'text/plain'});

        let a = document.createElement('a');

        a.href = URL.createObjectURL(file);
        a.download = me.props.tabs.reduce((x, y) => {
            if (y.props.idx == me.props.activeTab) {
                x = y.props.name;
            }
            return x;
        }, 'file-' + me.props.activeTab);

        a.download += '.txt';

        document.body.appendChild(a);

        a.dispatchEvent(new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
            view: window
        }));

        document.body.removeChild(a);
    }

    toggleTheme(ev, me) {
        me.set('theme', me.props.theme == 'app--light' ? 'app--dark' : 'app--light');

        me.props.save();
    }

    setText(ev, me) {
        let v = ev.target.value;
        let atIdx = app.props.getActiveTab(true);
        let tab = app.props.getActiveTab();

        tab.props.setTabName(v);
        tab.props.text = v;

        app.props.tabs[atIdx] = tab;
        app.props.toggleButtonInactive(v.length);
        
        me.props.text = v;
        me.props.autosize(ev, me);
        
        app.props.save();
    }

    autosize(ev) {
        const scrollTop = document.querySelector('html').scrollTop;

        this.height = 0;
        app.renderer.update();
        this.height = ev.target.scrollHeight;
        app.renderer.update();

        document.querySelector('html').scrollTo(0, scrollTop);
    }
}

class Tab {
    constructor(idx, name, text, activeClass) {
        this.idx = idx || (new Date()).getTime();
        this.name = name || '(new tab)';
        this.text = text || '';
        this.activeClass = activeClass || 'tab--active';
    }

    store() {
        return {
            idx: this.idx,
            activeClass: this.activeClass,
            name: this.name,
            text: this.text
        };
    }

    setTab(ev, me) {
        app.props.setActiveTab(parseInt(me.get('idx')));
        app.props.save();
        app.renderer.update();
    }

    remove(ev, me) {
        let tabs = app.get('tabs');

        if (tabs.length == 1) {
            return;
        }

        let nextActive = -1;

        tabs = tabs.reduce((x, y, z) => {
            if (y.props.idx != me.props.idx) {
                x.push(y);
            }
            else if (me.props.activeClass.length) {
                nextActive = z > 0 ? z - 1 : 0;
            }
            return x;
        }, []);

        app.set('tabs', tabs);

        if (nextActive > -1) {
            app.props.setActiveTab(tabs[nextActive].props.idx);
            app.renderer.update();
        }

        app.props.save();
    }

    setTabName(text) {
        let match = text.match(/^(.{0,8})(.*)/);
        let title = '(new tab)';

        if (match[1].length) {
            title = match[1];
            if (match[2].length)
                title += '...';
        }

        this.name = title;
    }
}

let main = new Flw('.main');
let app = new Elem('app', new App());

main.addElem(app);

main.run();

app.props.load();

app.props.autosize({target: document.querySelector('.text__area')});

window.addEventListener('resize', (ev) => {
    app.props.autosize({target: document.querySelector('.text__area')});
});

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js');
}